def test_users_post(client):
    response = client.post(
        "/users/", json={"email": "deadpool@example.com", "password": "chimichangas4life"},
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == "deadpool@example.com"
    assert "uuid" in data
    assert not "is_admin" in data
