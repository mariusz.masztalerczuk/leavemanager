def test_add_holiday(client, admin_token):

    headers = {"Authorization": "Bearer " + admin_token}
    response = client.post("/holidays/", headers=headers, json={"date": "2019-04-23T18:25:43.511Z"})
    data = response.json()
    assert response.status_code == 200


def test_get_holiday(client, admin_token):

    headers = {"Authorization": "Bearer " + admin_token}
    response = client.post("/holidays/", headers=headers, json={"date": "2019-04-23T18:25:43.511Z"})
    assert response.status_code == 200

    response = client.post("/holidays/", headers=headers, json={"date": "2019-04-30T18:25:43.511Z"})
    assert response.status_code == 200

    response = client.get("/holidays/", headers=headers)
    assert response.status_code == 200
    data = response.json()
    assert len(data) == 2
