from app.models import Leave


def test_leave(client, user_token):
    headers = {"Authorization": "Bearer " + user_token}
    response = client.post(
        "/leave/",
        headers=headers,
        json={"start_date": "2019-04-23T18:25:43.511Z", "end_date": "2019-04-25T18:25:43.511Z"},
    )
    assert response.status_code == 200


def test_leave_patch(client, user_token):
    headers = {"Authorization": "Bearer " + user_token}
    response = client.post(
        "/leave/",
        headers=headers,
        json={"start_date": "2019-04-23T18:25:43.511Z", "end_date": "2019-04-25T18:25:43.511Z"},
    )
    assert response.status_code == 200
    data = response.json()
    assert data["status"] == Leave.STATUS.DRAFT
    uuid = data["uuid"]

    response = client.patch(
        f"/leave/{uuid}", headers=headers, json={"status": Leave.STATUS.REQUESTED},
    )

    assert response.status_code == 200, response.text
    data = response.json()
    assert data["status"] == Leave.STATUS.REQUESTED


def test_get_all_my_leave(client, user_token):
    headers = {"Authorization": "Bearer " + user_token}
    response = client.post(
        "/leave/",
        headers=headers,
        json={"start_date": "2019-04-23T18:25:43.511Z", "end_date": "2019-04-25T18:25:43.511Z"},
    )
    assert response.status_code == 200
    response_1 = {
        "start_date": "2019-04-23T18:25:43.511000",
        "end_date": "2019-04-25T18:25:43.511000",
        "days": 2,
        "status": "DRAFT",
        "uuid": response.json()["uuid"],
    }
    headers = {"Authorization": "Bearer " + user_token}
    response = client.post(
        "/leave/",
        headers=headers,
        json={"start_date": "2020-04-23T18:25:43.511Z", "end_date": "2020-04-27T18:25:43.511Z"},
    )
    assert response.status_code == 200
    response_2 = {
        "start_date": "2020-04-23T18:25:43.511000",
        "end_date": "2020-04-27T18:25:43.511000",
        "days": 2,  # because saturday and sunday
        "status": "DRAFT",
        "uuid": response.json()["uuid"],
    }
    response = client.get("/leave/", headers=headers,)

    data = response.json()
    assert data == [response_1, response_2]
