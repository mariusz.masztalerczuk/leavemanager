def test_configured(client):
    response = client.get(
        "/status/"
    )
    assert response.status_code == 200
    data = response.json()
    assert data["status"] == "NOT CONFIGURED"

    response = client.post(
        "/create-admin-user/",
        json={"email": "deadpool@example.com", "password": "chimichangas4life"},
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == "deadpool@example.com"
    assert "uuid" in data
    assert data["is_admin"]

    response = client.get(
        "/status/"
    )

    assert response.status_code == 200
    data = response.json()
    assert data["status"] == "CONFIGURED"


def test_admin_users_post(client):
    response = client.post(
        "/create-admin-user/",
        json={"email": "deadpool@example.com", "password": "chimichangas4life"},
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == "deadpool@example.com"
    assert "uuid" in data
    assert data["is_admin"]


def test_try_to_add_second_admin(client):
    response = client.post(
        "/create-admin-user/",
        json={"email": "deadpool@example.com", "password": "chimichangas4life"},
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == "deadpool@example.com"
    assert "uuid" in data
    assert data["is_admin"]

    response = client.post(
        "/create-admin-user/",
        json={"email": "deadpool@example.com", "password": "chimichangas4life"},
    )

    assert response.status_code == 400


def test_try_to_admin_with_existing_email(client):
    response = client.post(
        "/users/", json={"email": "deadpool@example.com", "password": "chimichangas4life"},
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == "deadpool@example.com"
    assert "uuid" in data
    assert not "is_admin" in data

    response = client.post(
        "/create-admin-user/",
        json={"email": "deadpool@example.com", "password": "chimichangas4life"},
    )

    assert response.status_code == 400
