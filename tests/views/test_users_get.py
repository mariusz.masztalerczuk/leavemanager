def test_get_users_post_as_admin(client, admin_token):
    response = client.post(
        "/users/", json={"email": "deadpool1@example.com", "password": "chimichangas4life"},
    )
    assert response.status_code == 200

    response = client.post(
        "/users/", json={"email": "deadpool2@example.com", "password": "chimichangas4life"},
    )
    assert response.status_code == 200

    response = client.post(
        "/users/", json={"email": "deadpool3@example.com", "password": "chimichangas4life"},
    )
    assert response.status_code == 200

    headers = {"Authorization": "Bearer " + admin_token}

    response = client.get("/users/", headers=headers,)

    assert response.status_code == 200
    data = response.json()

    assert len(data) == 3


def test_users_post_as_manager(client, admin_token):
    response = client.post(
        "/users/", json={"email": "deadpool1@example.com", "password": "chimichangas4life"},
    )
    assert response.status_code == 200

    response = client.post(
        "/users/", json={"email": "deadpool2@example.com", "password": "chimichangas4life"},
    )
    assert response.status_code == 200

    response = client.post(
        "/users/", json={"email": "deadpool3@example.com", "password": "chimichangas4life"},
    )
    assert response.status_code == 200
    uuid = response.json()["uuid"]

    # login user
    response = client.post(
        "/login/", data={"username": "deadpool3@example.com", "password": "chimichangas4life"},
    )

    assert response.status_code == 200, response.text
    data = response.json()
    assert data["token_type"] == "bearer"
    manager_token = data["access_token"]

    headers = {"Authorization": "Bearer " + admin_token}
    client.patch(
        f"/users/{uuid}", headers=headers, json={"manager_uuid": uuid},
    )

    headers = {"Authorization": "Bearer " + manager_token}
    response = client.get("/users/", headers=headers,)

    assert response.status_code == 200
    data = response.json()

    assert len(data) == 1
