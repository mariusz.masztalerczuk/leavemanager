import config
from jose import jwt


def test_login_user(client):
    email = "deadpool@example.com"
    response = client.post("/users/", json={"email": email, "password": "chimichangas4life"})
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == "deadpool@example.com"
    assert "uuid" in data
    assert not "is_admin" in data

    response = client.post("/login/", data={"username": email, "password": "chimichangas4life"},)
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["token_type"] == "bearer"
    token = data["access_token"]

    decoded_token = jwt.decode(token, config.SECRET_KEY, config.ALGORITHM)
    assert decoded_token["sub"] == email
