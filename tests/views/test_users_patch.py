def test_users_patch(client, admin_token):
    email = "random@email.com"

    response = client.post("/users/", json={"email": email, "password": "eqweq"},)
    data = response.json()
    uuid = data["uuid"]

    headers = {"Authorization": "Bearer " + admin_token}
    response = client.patch(f"/users/{uuid}", headers=headers, json={"manager_uuid": uuid},)

    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == email
    assert data["manager_uuid"] == uuid
    assert not "is_admin" in data
