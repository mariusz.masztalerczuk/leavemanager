import tempfile

import pytest
import os
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from alembic.command import upgrade as alembic_upgrade
from alembic.config import Config as AlembicConfig

from app.database import get_db
from main import app


@pytest.fixture
def client():
    with tempfile.NamedTemporaryFile() as tmp:
        SQLALCHEMY_DATABASE_URL = f"sqlite:///{tmp.name}"

        engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False})
        TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

        alembic_config = AlembicConfig("alembic.ini")
        os.environ["DB_URI"] = SQLALCHEMY_DATABASE_URL
        alembic_upgrade(alembic_config, "head")

        def override_get_db():
            try:
                db = TestingSessionLocal()
                yield db
            finally:
                db.close()

        app.dependency_overrides[get_db] = override_get_db

        client = TestClient(app)
        yield client


@pytest.fixture
def user_token(client):
    email = "deadpool@example.com"

    # create user
    client.post(
        "/users/", json={"email": email, "password": "chimichangas4life"},
    )

    # login user
    response = client.post("/login/", data={"username": email, "password": "chimichangas4life"},)

    assert response.status_code == 200, response.text
    data = response.json()
    assert data["token_type"] == "bearer"
    return data["access_token"]


@pytest.fixture
def admin_token(client):
    email = "deadpool@example.com"

    # create user
    client.post(
        "/create-admin-user/", json={"email": email, "password": "chimichangas4life"},
    )

    # login user
    response = client.post("/login/", data={"username": email, "password": "chimichangas4life"},)

    assert response.status_code == 200, response.text
    data = response.json()
    assert data["token_type"] == "bearer"
    return data["access_token"]
