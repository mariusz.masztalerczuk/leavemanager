from datetime import timedelta

from typing import List
from fastapi import status
import config
import numpy as np
from fastapi.encoders import jsonable_encoder
from fastapi_mail import FastMail
from starlette.background import BackgroundTasks

from app.database import get_db
from fastapi import APIRouter, Depends, HTTPException
import app.schemas as schemas
from app.models import Leave, User
from sqlalchemy.orm import Session
from app.views.login import get_current_user

router = APIRouter()


def calculate_days(leave: schemas.LeaveCreate):
    return (int(np.busday_count(leave.start_date.date(), leave.end_date.date() + timedelta(days=1)))) * 8


@router.post("/leave/", response_model=schemas.Leave)
def create_leave(
    leave: schemas.LeaveCreate,
    db: Session = Depends(get_db),
    current_user=Depends(get_current_user),
):
    if not leave.is_half:
        days = calculate_days(leave)
    else:
        days = 4  # only 4h
        leave.end_date = leave.start_date
    return Leave.create_new_leave(user=current_user, db=db, leave=leave, days=days)


@router.delete("/leave/{uuid}", status_code=status.HTTP_204_NO_CONTENT)
def create_leave(
        uuid,
        db: Session = Depends(get_db),
        current_user=Depends(get_current_user),
):
    db_leave = Leave.get_user_by_uuid(db, uuid=uuid)

    if not db_leave:
        raise HTTPException(status_code=404, detail="Not found")

    if db_leave.user_id != current_user.id:
        raise HTTPException(status_code=401, detail="You have not permissions to remove that leave")

    if db_leave.status != Leave.STATUS.DRAFT:
        raise HTTPException(status_code=401, detail="Leave must be in DRAFT status")

    Leave.delete_leave(db, db_leave)

    return {}

template = """
<html> 
<body>
<p>Hi!
<br>New leave request is waiting for approve!</p> 
</body> 
</html>
"""


def send_notification(background_tasks, email):
    mail = FastMail(
        email=config.EMAIL,
        password=config.EMAIL_PASSWORD,
        tls=False,
        ssl=True,
        port="465",
        services=config.EMAIL_SERVER,
        custom=True,
    )
    background_tasks.add_task(
        mail.send_message, recipient=email, subject="New Leave", body=template, text_format="html"
    )


@router.patch("/leave/{uuid}", response_model=schemas.Leave)
def create_leave(
    uuid,
    leave: schemas.LeavePatch,
    background_tasks: BackgroundTasks,
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user),
):
    db_leave = Leave.get_user_by_uuid(db, uuid=uuid)
    is_send_notification_to_manager = (
        db_leave.status == Leave.STATUS.DRAFT and leave.status == Leave.STATUS.REQUESTED
    )

    if not db_leave:
        raise HTTPException(status_code=404, detail="Not found")

    # check if user has permission to change that
    updated_leave = Leave.update(db=db, db_leave=db_leave, new_values=jsonable_encoder(leave))

    if is_send_notification_to_manager and config.EMAIL_SERVICE_ENABLE:
        manager = User.get_user_by_uuid(db, uuid=current_user.manager_uuid)
        send_notification(background_tasks, manager.email)

    return updated_leave


@router.get("/leave/", response_model=List[schemas.Leave])
def get_leave(current_user: User = Depends(get_current_user),):
    return current_user.leaves
