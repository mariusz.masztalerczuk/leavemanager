from typing import List

from fastapi.encoders import jsonable_encoder

from app.database import get_db
from fastapi import APIRouter, HTTPException, Depends
import app.schemas as schemas
from app.models import User
from sqlalchemy.orm import Session

from app.views.login import get_current_user

router = APIRouter()


@router.post("/users/", response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = User.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return User.create_user_db(db=db, user=user)


@router.patch("/users/{uuid}", response_model=schemas.User)
def patch_user(
    uuid,
    user: schemas.UserPatch,
    db: Session = Depends(get_db),
    current_user=Depends(get_current_user),
):
    db_user = User.get_user_by_uuid(db, uuid=uuid)
    if not db_user:
        raise HTTPException(status_code=404, detail="Not found")
    if not current_user.is_admin:
        raise HTTPException(status_code=400, detail="User has not admin permission")
    return User.update(db=db, db_user=db_user, new_values=jsonable_encoder(user))


def get_all_users(db, users):
    configured_users = []
    for user in users:
        user.is_manager = len(User.get_all_underlings(db, uuid=user.uuid)) > 0
        user.leaves = [leave for leave in user.leaves if leave.status != 'DRAFT']
        configured_users.append(user)
    return configured_users


@router.get("/users/", response_model=List[schemas.UserInfo])
def get_users(
    current_user=Depends(get_current_user), db: Session = Depends(get_db),
):
    if current_user.is_admin:
        return get_all_users(db, User.get_all(db))
    return get_all_users(db, User.get_all_underlings(db, uuid=current_user.uuid))
