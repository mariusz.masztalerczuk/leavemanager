import config
from jose import JWTError, jwt
from datetime import timedelta, datetime
from app.database import get_db
from fastapi import APIRouter, Depends, status, HTTPException
import app.schemas as schemas
from app.models import User
from sqlalchemy.orm import Session
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")


def get_current_user(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, config.SECRET_KEY, algorithms=[config.ALGORITHM])
        email: str = payload.get("sub")
        if email is None:
            raise credentials_exception
    except JWTError:
        raise credentials_exception
    user = User.get_user_by_email(db, email)
    if user is None:
        raise credentials_exception
    return user


def create_new_token(user):
    access_token_expires = timedelta(minutes=config.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = {"sub": user.email, "exp": datetime.utcnow() + access_token_expires}

    if user.is_admin:
        access_token['admin'] = True

    if user.is_manager:
        access_token['manager'] = True

    return jwt.encode(access_token, config.SECRET_KEY, algorithm=config.ALGORITHM)


@router.post("/login/")
def login(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    db_user = User.get_user_by_email_and_password(
        db, email=form_data.username, password=form_data.password
    )
    if not db_user:
        raise HTTPException(status_code=400, detail="Incorrect credentials")
    db_user.is_manager = len(User.get_all_underlings(db, uuid=db_user.uuid)) > 0
    token = create_new_token(db_user)
    return {"access_token": token, "token_type": "bearer"}
