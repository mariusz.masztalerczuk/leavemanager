from typing import List

from app.database import get_db
from fastapi import APIRouter, Depends, HTTPException
import app.schemas as schemas
from sqlalchemy.orm import Session

from app.models.holidays import Holidays
from app.views.login import get_current_user

router = APIRouter()


@router.post("/holidays/", response_model=schemas.Holyday)
def create_leave(
        holiday: schemas.Holyday,
        db: Session = Depends(get_db),
        current_user=Depends(get_current_user),
):
    if not current_user.is_admin:
        raise HTTPException(status_code=400, detail="User has not admin permission")
    return Holidays.create_new_holidays(db=db, holiday=holiday)


@router.get("/holidays/", response_model=List[schemas.Holyday])
def create_leave(
        db: Session = Depends(get_db),
        current_user=Depends(get_current_user),
):
    if not current_user.is_admin:
        raise HTTPException(status_code=400, detail="User has not admin permission")

    return Holidays.get_all(db=db)