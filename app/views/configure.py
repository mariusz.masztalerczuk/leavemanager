from app.database import get_db
from fastapi import APIRouter, HTTPException, Depends
import app.schemas as schemas
from app.models import User
from sqlalchemy.orm import Session

router = APIRouter()


@router.post("/create-admin-user/", response_model=schemas.Admin)
def create_admin_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = User.get_admin_user(db)
    if db_user:
        raise HTTPException(status_code=400, detail="Admin already exists")

    db_user = User.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")

    return User.create_user_db(db=db, user=user, is_admin=True)


@router.get("/status", response_model=schemas.Status)
def create_admin_user(db: Session = Depends(get_db)):
    db_user = User.get_admin_user(db)
    if db_user:
        return schemas.Status(status="CONFIGURED")
    return schemas.Status(status="NOT CONFIGURED")
