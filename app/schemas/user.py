from datetime import datetime
from typing import Optional, List
from pydantic import BaseModel
import uuid


class Holyday(BaseModel):
    date: datetime
    class Config:
        orm_mode = True

class UserPatch(BaseModel):
    manager_uuid: Optional[uuid.UUID]


class UserCreate(BaseModel):
    email: str
    password: str
    manager_uuid: Optional[uuid.UUID]


class AdminCreate(UserCreate):
    is_admin: bool


class User(BaseModel):
    email: str
    uuid: uuid.UUID
    manager_uuid: Optional[uuid.UUID]

    class Config:
        orm_mode = True


class LeaveCreate(BaseModel):
    start_date: datetime
    end_date: datetime
    is_half: Optional[bool]

class Admin(User):
    is_admin: bool


class Status(BaseModel):
    status: str


class LeavePatch(BaseModel):
    status: str


class Leave(LeaveCreate):
    days: int
    status: str
    uuid: uuid.UUID

    class Config:
        orm_mode = True


class UserInfo(User):
    is_admin: bool
    is_manager: bool
    leaves: List[Leave]


class Token(BaseModel):
    access_token: str
    token_type: str
