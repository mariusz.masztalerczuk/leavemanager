import uuid

from sqlalchemy import Column, Integer, ForeignKey, DateTime, String
from sqlalchemy.orm import Session

from app import schemas
from app.database import Base
from app.models.guid import GUID


class Leave(Base):
    class STATUS:
        DRAFT = "DRAFT"
        REQUESTED = "REQUESTED"
        ACCEPTED = "ACCEPTED"
        REFUSED = "REFUSED"

    __tablename__ = "leaves"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("users.id", ondelete="CASCADE"))
    uuid = Column(GUID(), nullable=False, index=True)
    start_date = Column(DateTime, nullable=False)
    end_date = Column(DateTime, nullable=False)
    days = Column(Integer, nullable=False)
    status = Column(String, nullable=False)

    @staticmethod
    def delete_leave(db, db_leave):
        db.delete(db_leave)
        db.commit()


    @staticmethod
    def get_user_by_uuid(db: Session, uuid: str):
        return db.query(Leave).filter(Leave.uuid == uuid).first()

    @staticmethod
    def get_all_by_user_uuid(db: Session, uuid: str):
        return db.query(Leave).filter(Leave.uuid == uuid).all()

    @staticmethod
    def create_new_leave(user, db: Session, leave: schemas.LeaveCreate, days: int):
        db_leave = Leave(
            user_id=user.id,
            start_date=leave.start_date,
            end_date=leave.end_date,
            days=days,
            status=Leave.STATUS.DRAFT,
            uuid=str(uuid.uuid4()),
        )
        db.add(db_leave)
        db.commit()
        db.refresh(db_leave)
        return db_leave

    @staticmethod
    def update(db: Session, db_leave, new_values):
        for key in new_values:
            setattr(db_leave, key, new_values[key])
        db.commit()
        return db_leave
