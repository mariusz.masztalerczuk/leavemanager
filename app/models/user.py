import uuid
from sqlalchemy import Boolean, Column, Integer, String

from app.database import Base
from sqlalchemy.orm import Session, relationship, backref
import app.schemas as schemas
from .leave import Leave
from app.models.guid import GUID


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(GUID(), nullable=False, index=True)
    manager_uuid = Column(GUID(), nullable=True)
    email = Column(String, unique=True, index=True)
    is_admin = Column(Boolean, default=False)
    hashed_password = Column(String)

    leaves = relationship(
        Leave, passive_deletes=True, uselist=True, backref=backref("users", uselist=False),
    )

    @staticmethod
    def get_all(db: Session):
        return db.query(User).all()

    @staticmethod
    def get_all_underlings(db: Session, uuid):
        return db.query(User).filter(User.manager_uuid == uuid).all()

    @staticmethod
    def get_admin_user(db: Session):
        return db.query(User).filter(User.is_admin == True).first()

    @staticmethod
    def get_user_by_email(db: Session, email: str):
        return db.query(User).filter(User.email == email).first()

    @staticmethod
    def get_user_by_uuid(db: Session, uuid: str):
        return db.query(User).filter(User.uuid == uuid).first()

    @staticmethod
    def get_user_by_email_and_password(db: Session, email: str, password: str):
        return db.query(User).filter(User.email == email, User.hashed_password == password).first()

    @staticmethod
    def create_user_db(db: Session, user: schemas.UserCreate, is_admin=False):
        fake_hashed_password = user.password
        db_user = User(
            email=user.email,
            hashed_password=fake_hashed_password,
            is_admin=is_admin,
            uuid=str(uuid.uuid4()),
        )
        db.add(db_user)
        db.commit()
        db.refresh(db_user)
        return db_user

    @staticmethod
    def update(db: Session, db_user, new_values):
        for key in new_values:
            setattr(db_user, key, new_values[key])
        db.commit()
        return db_user
