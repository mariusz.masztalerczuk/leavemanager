from sqlalchemy import Column, Integer, DateTime
from sqlalchemy.orm import Session

from app import schemas
from app.database import Base
from app.models.guid import GUID
import uuid


class Holidays(Base):
    __tablename__ = "holidays"

    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(GUID(), nullable=False, index=True)
    date = Column(DateTime, nullable=False)

    @staticmethod
    def get_all(db: Session):
        return db.query(Holidays).all()

    @staticmethod
    def create_new_holidays(db: Session, holiday: schemas.UserCreate):
        db_holiday = Holidays(
            date=holiday.date,
            uuid=str(uuid.uuid4()),
        )
        db.add(db_holiday)
        db.commit()
        db.refresh(db_holiday)
        return db_holiday