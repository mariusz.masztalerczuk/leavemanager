ACCESS_TOKEN_EXPIRE_MINUTES = 60 * 24
SECRET_KEY = "RANDOM_TEXT"
ALGORITHM = "HS256"

SQLALCHEMY_DATABASE_URL = "postgresql://test:test@localhost/test"

# email
EMAIL_SERVICE_ENABLE = False
EMAIL = "pkh@foo.wtf"
EMAIL_PASSWORD = "password"
EMAIL_SERVER = "mail6.mydevil.net"
