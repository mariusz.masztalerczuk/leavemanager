from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app.views import users, configure, leave, login,holidays

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(users.router)
app.include_router(configure.router)
app.include_router(leave.router)
app.include_router(login.router)
app.include_router(holidays.router)
